﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PetaPocoExample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            RunDB();
        }

        private void RunDB()
        {
            using (MyConn.Instance)
            {
                try
                {
                    var result = MyConn.Instance.Query<article>("select * from article").ToList();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }

    /// <summary>
    /// Employee class, using properties for PetaPoco
    /// </summary>
    [TableName("article")]
    [PrimaryKey("ID")]
    public class article
    {
        public int ID { get; set; }
        public string Title { get; set; }
    }
}
