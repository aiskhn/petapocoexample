﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetaPocoExample
{
    //Singleton
    public sealed class MyConn
    {
        //private static Database instance = null;
        //private static readonly object padlock = new object();

        //MyConn()
        //{
        //}

        //public static Database Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //        {
        //            lock (padlock)
        //            {
        //                if (instance == null)
        //                {
        //                    instance = new Database("MySQL");
        //                }
        //            }
        //        }

        //        return instance;
        //    }
        //}

        private static readonly Lazy<Database> lazy = new Lazy<Database>(() => new Database("MySQL"));

        public static Database Instance { get { return lazy.Value; } }

        private MyConn()
        {
        }
    }
}
